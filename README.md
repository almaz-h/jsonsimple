# JsonSimple

The program calculates the average flight time of various flights between the cities of Vladivostok and Tel Aviv.
1. Задача 1: найти среднее время перелета из города Владивосток в Тель-Авив 
из файла с данными в формате JSON - там списко объектов (tickets.json).
2. Задача 2: найти 90-ый перцентиль времени перелета.
3. Программа сборка и запуск через мавен - mvn exec:java -Dexec.mainClass="\classpath" .
4. Пример выполнения:

![Image](src/main/resources/image/JsonSimpleAnswer.png)
