package ru.khalitov.JsonSimple;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by almaz-h!
 */
public class FlightTimeLogic {

    private static final String jsonFileName = "tickets.json";
    /**
     * @param jsonDepartureTimeField, необходимая нам для расчетов строка. Это время вылета из г.Владивосток
     */
    private static final String jsonDepartureTimeField = "departure_time";
    /**
     * @param jsonArrivalTimeField, необходимая нам для расчетов строка. Это время прилета в г.Тель-Авив
     */
    private static final String jsonArrivalTimeField = "arrival_time";
    private static final List<String> depTimeList = new ArrayList<>();
    private static final List<String> arTimeList = new ArrayList<>();
    private static final List<Integer> flightTimeList = new ArrayList<>(); // Храним время полета в минутах
    private static final Map<Integer, Integer> mapForCalc = new HashMap<>(); // хэш-мапа для расчета перцентиля
    /**
     * ответ - среднее время перелета в формате часы:минуты
     */
    private static String averageTravelTime;
    /**
     * ответ - 90-ый перцентиль времени перелета в формате часы:минуты
     */
    private static String percentileValue;

    public static void main(String[] args) {
        readJsonFile();
        flightTimeToList();
        int avValueMinutes = averageValueInTravel();
        System.out.println("Average travel time in minutes: " + avValueMinutes);
        averageTravelTime = timeInMinToHourString(avValueMinutes);
        System.out.println("Average travel time in HH:MM format: " + averageTravelTime);
        int rangPercentile = calcPercentiles();
        percentileValue = findPercentileValue(rangPercentile);
        System.out.println("90 percentile in travel time is: " + percentileValue);
    }

    private static void readJsonFile() {
        File jsonFile = new File(jsonFileName).getAbsoluteFile();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = null;
        try {
            jsonNode = mapper.readTree(jsonFile);
        } catch (IOException e) {
            System.out.println("Data not found!");
            System.exit(404);
        }
        List<JsonNode> departureTimeNodes = jsonNode.findValues(jsonDepartureTimeField);
        List<JsonNode> arrivalTimeNodes = jsonNode.findValues(jsonArrivalTimeField);

        departureTimeNodes.forEach(node -> depTimeList.add(node.asText()));
        arrivalTimeNodes.forEach(node -> arTimeList.add(node.asText()));

        System.out.println("Departure time: " + depTimeList.toString());
        System.out.println("Arrival time: " + arTimeList.toString());
    }

    private static void flightTimeToList() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.forLanguageTag("RU"));
        Date dTime = null;
        Date aTime = null;
        for (int i = 0; i < depTimeList.size(); i++) {
            String lineDepTime = depTimeList.get(i);
            String lineArTime = arTimeList.get(i);

            try {
                dTime = format.parse(lineDepTime);
                aTime = format.parse(lineArTime);
            } catch (ParseException e) {
                System.out.println("Wrong format");
            }
            long difference = 0;
            if (aTime != null && dTime != null) {
                difference = aTime.getTime() - dTime.getTime();
            }
            int diffMinutes = (int) difference / (60 * 1000); // время перелета в минутах
            flightTimeList.add(diffMinutes);
        }
        System.out.println("Flight Time: " + flightTimeList);
    }

    /*
       метод вычисляет среднее значение в коллекции flightTimeList, которая хранит числовые значения
       времени перелета всех рейсов в минутах
        */
    private static int averageValueInTravel() {
        OptionalDouble average = flightTimeList.stream().
                mapToInt(time -> time).
                average();
        return (int) average.orElse(-1);
    }

    private static String timeInMinToHourString(int avValueMinutes) {
        int miInHour = 60;
        int hour = avValueMinutes / miInHour,
                min = avValueMinutes % miInHour;
        return String.format("%02d:%02d", hour, min);
    }

    /**
     * В соответствии с определением, P-й процентиль списка из N упорядоченных по величине чисел
     * (от меньших к большим) является наименьшее в списке число, которое больше,
     * чем N процентов всех чисел исследуемого ряда.Для расчета процентиля необходимо расположить значения в наборе данных
     * по порядку от меньшего к большему и каждому значению из набора данных присвоить порядковый номер (ранг).
     * Затем рассчитывается порядковый номер n для заданного процентиля по формуле: n = P/100 * N , 0<P<100
     *
     * @return ранг - порядковый номер
     */
    private static int calcPercentiles() {
        Collections.sort(flightTimeList);
        int listSize = flightTimeList.size();
        int percentile = 90;
        int percMax = 100;
        for (int i = 0; i < listSize; i++) {
            mapForCalc.put(i + 1, flightTimeList.get(i));
        }
        return (percentile * listSize) / percMax;
    }

    private static String findPercentileValue(int rangPercentile) {
        int valInRang = mapForCalc.get(rangPercentile);
        return timeInMinToHourString(valInRang);
    }
}




